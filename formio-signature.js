'use strict';

const bluebird = require('bluebird');
const moment = require('moment');
const superagent = require('superagent');
const urljoin = require('url-join');


/**
 * Determine if a form needs to be signed
 *
 * @param {object} signature signature data model
 * @param {object} auth authentication module
 * @param {object} logger logging module
 * @param {function} config configuration data
 * @returns {object} Methods to determine if a form needs to be signed
 */
module.exports = (signature, auth, logger, config) => {
  if (signature == null) {
    throw new Error('missing signature model');
  }

  if (auth == null) {
    throw new Error('missing authorization object');
  }

  if (logger == null) {
    throw new Error('missing logger object');
  }

  if (config == null) {
    throw new Error('missing configuration object');
  }

  function formRequiresSignature(formSchema) {
    let required = false;

    const container = formSchema.components.find((component) => {
      return component.key === 'hiddenContainer';
    });

    if (container != null) {
      const lastModified = container.components.find((component) => {
        return component.key === 'signedDuration';
      });

      required = lastModified != null;
    }

    return required;
  }

  function getSignedDate(formName, clientId, personId) {
    return signature.collection().query({
      where: {
        formName: formName,
        clientId: clientId,
        personId: personId
      }
    })
    .orderBy('signedDateTime', 'DESC')
    .fetchOne()
    .then((model) => {
      return model == null ? null : model.get('signedDateTime');
    });
  }

  function getFormSchema(formName) {
    return bluebird.try(() => {
      const uri = urljoin(config('FORMIO_PROJECT_URL'), formName);
      return auth.getServiceUserToken()
        .then((token) => {
          return superagent.get(uri)
            .set({ 'x-jwt-token': token });
        });
    })
    .then((response) => {
      return response.body;
    })
    .catch((error) => {
      logger.error(error);
      return bluebird.reject(error);
    });
  }

  function modifiedSinceSigned(formSchema, lastSigned) {
    const container = formSchema.components.find((component) => {
      return component.key === 'hiddenContainer';
    });

    const lastModified = container.components.find((component) => {
      return component.key === 'lastModifiedDate';
    });

    return moment(lastSigned).diff(lastModified.defaultValue) < 0;
  }

  function signatureExpired(formSchema, lastSigned) {
    const container = formSchema.components.find((component) => {
      return component.key === 'hiddenContainer';
    });

    const duration = container.components.find((component) => {
      return component.key === 'signedDuration';
    });

    const daysRemaining = moment(lastSigned)
      .add(duration.defaultValue, 'days')
      .diff(moment(), 'days');

    return daysRemaining < 0;
  }

  function needsSignature(formName, clientId, personId) {
    return getFormSchema(formName)
      .then((formSchema) => {
        const required = formRequiresSignature(formSchema);

        if (required === false) {
          return false;
        }

        return getSignedDate(formName, clientId, personId)
          .then((lastSigned) => {
            if (lastSigned == null) {
              return true;
            }

            const modified = modifiedSinceSigned(formSchema, lastSigned);
            const expired = signatureExpired(formSchema, lastSigned);

            return modified === true || expired === true;
          });
      })
      .catch((error) => {
        logger.error(error);
        return bluebird.reject(error);
      });
  }

  return {
    needsSignature: needsSignature
  };
};

