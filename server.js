'use strict';

const hapi = require('hapi');
const logging = require('@premise/hapi-logging');
const inert = require('inert');
const premiseAuth = require('@premise/premise-auth');
const statusPlugin = require('@premise/status-route');
const swagger = require('hapi-swagger');
const vision = require('vision');

module.exports = (pack, routes, logger, config, db) => {
  const server = new hapi.Server();

  server.connection({
    host: config('HOST'),
    port: config('PORT')
  });

  const plugins = [
    vision,
    inert, {
      register: logging.register,
      options: {
        'console': {
          args: {},
          enabled: config('CONSOLE_LOG_ENABLED')
        },
        file: {
          args: config('LOG_LOCATION'),
          enabled: config('FILE_LOG_ENABLED')
        }
      }
    },
    premiseAuth, {
      register: swagger,
      options: {
        info: {
          title: 'Test API Documentation',
          version: pack.version
        }
      }
    }, {
      register: statusPlugin,
      options: {
        version: pack.version,
        knexDb: db,
        customConfigs: [
          { key: 'FORMIO_URL', value: config('FORMIO_URL') },
          { key: 'FORMIO_PROJECT_URL', value: config('FORMIO_PROJECT_URL') },
          { key: 'FORMIO_SERVICE_ACCOUNT_EMAIL', value: config('FORMIO_SERVICE_ACCOUNT_EMAIL') },
          { key: 'REDIS_HOST', value: config('REDIS_HOST') },
          { key: 'REDIS_PORT', value: config('REDIS_PORT') }
        ]
      }
    }
  ];

  server.register(plugins, (error) => {
    if (error) {
      logger.error(`Failed loading plugins: ${error}`);
      process.exit(1);
    }
    server.route(routes);
  });

  return server;
};
