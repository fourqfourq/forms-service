'use strict';

const bluebird = require('bluebird');
const superagent = require('superagent');
const urljoin = require('url-join');

/**
 * Module to complete form.io authentication for service account
 *
 * @param  {object} cache - cache module
 * @param  {object} logger - logging module
 * @param  {object} config - config module
 * @return {object} Object containing method to complete form.io auth
 */
module.exports = (cache, logger, config) => {
  if (cache == null) {
    throw new Error('missing cache object');
  }

  if (logger == null) {
    throw new Error('missing logger object');
  }

  if (config == null) {
    throw new Error('missing configuration object');
  }

  const email = config('FORMIO_SERVICE_ACCOUNT_EMAIL');
  const password = config('FORMIO_SERVICE_ACCOUNT_PASSWORD');
  const serviceTokenCacheKey = config('CACHE_KEY_SERVICE_TOKEN');
  const userLoginUri = urljoin(
      config('FORMIO_URL'),
      config('FORMIO_ROUTE_SERVICE_USER_LOGIN'));

  function getServiceUserToken() {
    return cache.get(serviceTokenCacheKey)
      .then((cachedToken) => {
        if (cachedToken == null) {
          return bluebird.try(() => {
            return superagent
              .post(userLoginUri)
              .send({ data: { email: email, password: password } });
          })
          .then((response) => {
            const token = response.headers['x-jwt-token'];
            if (token != null) {
              cache.set(serviceTokenCacheKey, token);
            }
            return token;
          })
          .catch((error) => {
            logger.error(error);
            return bluebird.reject(error);
          });
        }
        return cachedToken;
      });
  }

  return {
    getServiceUserToken: getServiceUserToken
  };
};
