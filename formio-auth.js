'use strict';

/**
 * Authorization utilities
 *
 * @param {object} formioServiceAuth - form.io service auth module
 * @param {object} formioUserAuth - form.io user auth module
 * @returns {object} object of utility functions
 */
module.exports = (formioServiceAuth, formioUserAuth) => {
  if (formioServiceAuth == null) {
    throw new Error('missing form.io service auth object');
  }

  if (formioUserAuth == null) {
    throw new Error('missing form.io user auth object');
  }

  return {
    getServiceUserToken: formioServiceAuth.getServiceUserToken,
    getUserJwt: formioUserAuth.getUserJwt
  };
};
