/* eslint no-console:0 */
/* eslint max-len:0 */
'use strict';

const Auth = require('./formio-auth');
const Crypt = require('./crypt');
const Database = require('./database');
const FormGetUserSubmissionHandler = require('./handlers/form-getUserSubmission');
const FormGetUserSubmissionRoute = require('./routes/form-getUserSubmission');
const FormioAuthServiceAccount = require('./formio-service-auth');
const FormioAuthUserAccount = require('./formio-user-auth');
const FormsGetByTagHandler = require('./handlers/forms-getByTag');
const FormsGetByTagRoute = require('./routes/forms-getByTag');
const Logger = require('./logger');
const Redis = require('./redis');
const Server = require('./server');
const UserCreateHandler = require('./handlers/user-create');
const UserCreateRoute = require('./routes/user-create');
const UserModel = require('./models/user');
const UserSubmissionHandler = require('./handlers/user-submission');
const UserSubmissionRoute = require('./routes/user-submission');
const config = require('./config');
const pack = require('./package.json');

const logger = Logger(config);
const crypt = Crypt(config);
const db = Database(config);
const cache = Redis(logger, config);
const user = UserModel(db);
const formioAuthServiceAccount = FormioAuthServiceAccount(cache, logger, config);
const formioAuthUserAccount = FormioAuthUserAccount(formioAuthServiceAccount, logger, config, user, cache);
const auth = Auth(formioAuthServiceAccount, formioAuthUserAccount);

const userCreateHandler = UserCreateHandler(cache, user, auth, logger, config, crypt);
const userSubmissionHandler = UserSubmissionHandler(cache, user, auth, logger, config);
const formsGetByTagHandler = FormsGetByTagHandler(auth, logger, config);
const formGetUserSubmissionHandler = FormGetUserSubmissionHandler(auth, logger, config);
const routes = [
  UserCreateRoute({
    path: '/user/create',
    method: 'POST',
    handler: userCreateHandler
  }),
  UserSubmissionRoute({
    path: '/{formName}/submission',
    method: 'POST',
    handler: userSubmissionHandler
  }),
  FormsGetByTagRoute({
    path: '/forms',
    method: 'GET',
    handler: formsGetByTagHandler
  }),
  FormGetUserSubmissionRoute({
    path: '/form/{formName}/submission',
    method: 'GET',
    handler: formGetUserSubmissionHandler
  })
];
const server = Server(pack, routes, logger, config, db);

server.start(() => {
  logger.info(`Server running at: ${server.info.uri}`);
});

module.exports = server;
