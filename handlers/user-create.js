/* eslint no-underscore-dangle: 0 */
'use strict';

const boom = require('boom');
const bluebird = require('bluebird');
const random = require('randomstring');
const uuid = require('node-uuid');
const superagent = require('superagent');
const urljoin = require('url-join');

/**
 * Creates a form.io user
 *
 * @param {object} cache - caching module
 * @param {object} user - user model
 * @param {object} auth - service account authorization module
 * @param {object} logger - logging module
 * @param {function} config - configuration module
 * @param {object} crypt - crypto module for password encrypt/decrypt
 * @returns {function} - hapi handler
 */
module.exports = (cache, user, auth, logger, config, crypt) => {
  if (cache == null) {
    throw new Error('missing cache object');
  }

  if (user == null) {
    throw new Error('missing user object');
  }

  if (auth == null) {
    throw new Error('missing authorization object');
  }

  if (logger == null) {
    throw new Error('missing logger object');
  }

  if (config == null) {
    throw new Error('missing configuration object');
  }

  if (crypt == null) {
    throw new Error('missing crypt object');
  }

  const createUserUri = urljoin(
    config('FORMIO_PROJECT_URL'),
    config('FORMIO_ROUTE_USER_SUBMISSION'));
  const cacheSeconds = config('REDIS_EXPIRE_SECONDS');

  /**
   * Hapijs handler
   *
   * @param {object} request http request
   * @param {object} reply http response
   * @returns {object}
   *
   * Sets status code to 201 if user is created,
   * else 200 if already exists.
   */
  return (request, reply) => {
    const email = `${uuid.v4()}@${uuid.v4()}.com`;
    const password = random.generate({ length: 15 });
    const personId = request.headers.personid;
    const clientId = request.headers.clientid;
    const cacheKey = `${clientId}_${personId}`;

    function handleUser() {
      return bluebird.try(() => {
        return user.where({
          personId: personId,
          clientId: clientId
        })
        .fetch()
        .then((model) => {
          const formioUserId = model == null ? null : model.get('formioUserId');
          return [auth.getServiceUserToken(), formioUserId];
        });
      })
      .spread((serviceToken, formioUserId) => {
        if (formioUserId == null) {
          return superagent.post(createUserUri)
            .send({ data: { email: email, password: password } })
            .set({ 'x-jwt-token': serviceToken })
            .then((response) => [response.headers['x-jwt-token'], response.body._id, true]);
        }

        return superagent.get(`${createUserUri}/${formioUserId}`)
          .send({ userId: formioUserId })
          .set({ 'x-jwt-token': serviceToken })
          .then((response) => [response.headers['x-jwt-token'], response.body._id, false]);
      })
      .spread((userToken, formioUserId, isNewUser) => {
        if (isNewUser === true) {
          user.forge({
            clientId: clientId,
            personId: personId,
            email: email,
            password: crypt.encrypt(password),
            formioUserId: formioUserId
          })
          .save();
        }

        cache.set(cacheKey, userToken);
        cache.expire(cacheKey, cacheSeconds);
        return reply()
          .header('x-jwt-token', userToken)
          .code(isNewUser === true ? 201 : 200);
      });
    }

    return cache.get(cacheKey)
      .then((token) => {
        if (token == null) {
          return handleUser();
        }

        return reply()
          .header('x-jwt-token', token)
          .code(200);
      })
      .catch((error) => {
        logger.error(error);
        return reply(boom.wrap(error, error.status));
      });
  };
};
