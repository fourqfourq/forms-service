'use strict';

const lodashArray = require('lodash/array');
const bluebird = require('bluebird');
const boom = require('boom');
const superagent = require('superagent');
const urljoin = require('url-join');

/**
 * Retrieves a collection of forms by an tag
 * @param  {object} auth - service account authorization module
 * @param  {object} logger - logging module
 * @param  {function} config - configuration module
 * @return {function} - hapi handler
 */
module.exports = (auth, logger, config) => {
  if (auth == null) {
    throw new Error('missing authorization object');
  }

  if (logger == null) {
    throw new Error('missing logger object');
  }

  if (config == null) {
    throw new Error('missing configuration object');
  }

  /**
   * Retrieve forms by tag
   *
   * @param  {string} tag - tag to lookup forms for in form.io
   * @param  {string} token - token used for authentication in form.io
   * @return {array} - The forms
   */
  function requestFormsByTag(tag, token) {
    const uri = urljoin(config('FORMIO_PROJECT_URL'), config('FORMIO_ROUTE_FORM_GET'));


    return superagent
      .get(`${uri}?tags=${tag}`)
      .set('x-jwt-token', token)
      .then((response) => {
        return response.body;
      });
  }

  return (request, reply) => {
    return auth.getServiceUserToken()
      .then((token) => {
        if (token == null) {
          throw new Error('missing authorization token');
        }

        let tags = [];

        if (Array.isArray(request.query.tag)) {
          tags = request.query.tag;
        } else {
          tags.push(request.query.tag);
        }

        return bluebird.map(tags, (tag) => {
          return requestFormsByTag(tag, token);
        })
        .catch((error) => {
          if (error.statusCode === 404) {
            return [];
          }
          throw error;
        })
        .filter((tagForms) => {
          return tagForms.length > 0;
        })
        .then((forms) => {
          const flatForms = lodashArray.uniqBy(lodashArray.flatten(forms), '_id');
          return reply(flatForms).code(200);
        })
        .catch((error) => {
          if (error.statusCode === 401) {
            return reply(boom.unauthorized('Unauthorized'));
          }
          return reply(boom.badRequest(error.message));
        });
      })
      .catch((error) => {
        logger.error(error);
        return reply(boom.badRequest(error.message));
      });
  };
};
