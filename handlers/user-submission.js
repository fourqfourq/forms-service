'use strict';

const boom = require('boom');
const bluebird = require('bluebird');

/**
 * Submits a form.io submission
 *
 * @param {object} cache - caching module
 * @param {object} user - user model
 * @param {object} auth - service account authorization module
 * @param {object} logger - logging module
 * @param {function} config - configuration module
 * @param {object} crypt - crypto module for password encrypt/decrypt
 * @returns {function} - hapi handler
 */
module.exports = (cache, user, auth, logger, config) => {
  if (cache == null) {
    throw new Error('missing cache object');
  }

  if (user == null) {
    throw new Error('missing user object');
  }

  if (auth == null) {
    throw new Error('missing authorization object');
  }

  if (logger == null) {
    throw new Error('missing logger object');
  }

  if (config == null) {
    throw new Error('missing configuration object');
  }

  return (request, reply) => {
    const personId = request.headers.personid;
    const clientId = request.headers.clientid;
    const formName = request.params.formName;
    const submission = request.payload;

    logger.info(`formName: ${formName}`);
    logger.info(`personId: ${personId}`);
    logger.info(`submission: ${submission}`);

    return bluebird.try(() => {
      return user.where({
        personId: personId,
        clientId: clientId
      })
      .fetch()
      .then((model) => {
        if (model == null) {
          throw boom.notFound('User not found');
        }

        logger.info(`formioUserId ${model.get('formioUserId')}`);
        return [auth.getUserJwt(personId, clientId), model.get('formioUserId')];
      });
    })
    .spread((userToken, formioUserId) => {
      // TODO: submit to form.io
      logger.info(`formioUserId: ${formioUserId}`);
      logger.info(`userToken: ${userToken}`);
      return 'response';
    })
    .then((response) => {
      // TODO:
      // if form.io submission post response is successful
      //    parse submission
      // else
      //    reply with badRequest
      // end
      logger.info(`response: ${response}`);
      return reply().code(201);
    })
    .catch((error) => {
      logger.error(error);
      return reply(boom.wrap(error, error.status));
    });
  };
};
