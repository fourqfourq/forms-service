'use strict';

const boom = require('boom');
const superagent = require('superagent');

/**
 * Handler to get a form.io user submission by form
 *
 * @param  {object} auth - authentication module
 * @param  {object} logger - logging module
 * @param  {object} config - configuration module
 * @return {function} - hapi handler
 */
module.exports = (auth, logger, config) => {
  if (auth == null) {
    throw new Error('missing authorization object');
  }

  if (logger == null) {
    throw new Error('missing logger object');
  }

  if (config == null) {
    throw new Error('missing configuration object');
  }

  return (request, reply) => {
    const personId = request.headers.personid;
    const clientId = request.headers.clientid;
    const formName = request.params.formName;
    return auth.getUserJwt(personId, clientId)
      .then((userToken) => {
        const formioProjectUri = config('FORMIO_PROJECT_URL');
        const formioSubmissionUri = config('FORMIO_ROUTE_FORM_GET_SUBMISSION');
        const formioUserSubmissionUrl =
          `${formioProjectUri}/${formName}${formioSubmissionUri}?sort=-modified&limit=1`;

        return superagent
          .get(formioUserSubmissionUrl)
          .set({
            'x-jwt-token': userToken,
            'Range-Unit': 'items',
            'Range': '0-20'
          })
          .then((response) => {
            reply(response.body);
          });
      })
      .catch((error) => {
        logger.error(error);
        return reply(boom.wrap(error));
      });
  };
};
