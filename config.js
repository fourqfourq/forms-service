'use strict';

const nconf = require('nconf');
const path = require('path');

nconf.env()
  .add('local-env', {
    type: 'file',
    file: path.join(__dirname, 'config', `${nconf.get('NODE_ENV')}.json`)
  })
  .add('defaults', {
    type: 'file',
    file: path.join(__dirname, 'config', 'defaults.json')
  })
  .load();

module.exports = (key) => {
  return nconf.get(key);
};

