'use strict';

const winston = require('winston');

module.exports = (config) => {
  const transports = [];

  winston.emitErrs = true;

  transports.push(new winston.transports.File({
    level: config('FILE_LOG_LEVEL'),
    filename: config('LOG_LOCATION'),
    handleExceptions: true,
    json: true,
    maxsize: config('FILE_LOG_MAXSIZE'),
    maxFiles: config('FILE_LOG_MAXFILES'),
    colorize: true
  }));

  transports.push(new winston.transports.Console({
    level: config('CONSOLE_LOG_LEVEL'),
    handleExceptions: true,
    json: false,
    colorize: true
  }));

  return new winston.Logger({
    transports: transports,
    exitOnError: false
  });
};
