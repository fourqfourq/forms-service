FROM premisehealth-docker-dockerv2-local.artifactoryonline.com/nodejs:v4.4.7

ENV SERVICE_NAME node-service

RUN mkdir /apps/${SERVICE_NAME}
RUN mkdir /apps/${SERVICE_NAME}/log
COPY . /apps/${SERVICE_NAME}

EXPOSE 8080

WORKDIR /apps/${SERVICE_NAME}

CMD ["npm", "start"]
