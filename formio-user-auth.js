'use strict';

const bluebird = require('bluebird');
const boom = require('boom');
const superagent = require('superagent');
const urljoin = require('url-join');

/**
 * Module to complete form.io authentication for a user
 *
 * @param  {object} formioServiceAuth - Object used to complete form.io
 *   authentication for the service account
 * @param  {object} logger - logging module
 * @param  {object} config - configuration module
 * @param  {object} user - user module
 * @param  {object} cache - cache module
 * @return {object} Object containing method to complete user auth
 */
module.exports = (formioServiceAuth, logger, config, user, cache) => {
  if (formioServiceAuth == null) {
    throw new Error('missing formioServiceAuth object');
  }

  if (logger == null) {
    throw new Error('missing logger object');
  }

  if (config == null) {
    throw new Error('missing configuration object');
  }

  if (user == null) {
    throw new Error('missing user object');
  }

  if (cache == null) {
    throw new Error('missing cache object');
  }

  const createUserUri = urljoin(
    config('FORMIO_PROJECT_URL'),
    config('FORMIO_ROUTE_USER_SUBMISSION'));
  const cacheSeconds = config('REDIS_EXPIRE_SECONDS');

  function getUserJwt(personId, clientId) {
    const cacheKey = `${clientId}_${personId}`;

    function lookupPerson() {
      return bluebird.try(() => {
        return user.where({
          personId: personId,
          clientId: clientId
        })
        .fetch()
        .then((model) => {
          const formioUserId = model == null ? null : model.get('formioUserId');
          return formioUserId;
        });
      });
    }

    function requestUserToken(serviceAccountToken) {
      return lookupPerson(personId)
        .then((formioUserId) => {
          if (formioUserId == null) {
            throw boom.badRequest('user has not been created in form.io');
          }

          return superagent.get(`${createUserUri}/${formioUserId}`)
            .send({ userId: formioUserId })
            .set({ 'x-jwt-token': serviceAccountToken })
            .then((response) => {
              const userToken = response.headers['x-jwt-token'];
              if (userToken == null) {
                throw boom.unauthorized('failed to get user token from form.io');
              }

              cache.set(cacheKey, userToken);
              cache.expire(cacheKey, cacheSeconds);
              return userToken;
            });
        });
    }

    return cache.get(cacheKey)
      .then((cachedToken) => {
        if (cachedToken == null) {
          return formioServiceAuth.getServiceUserToken()
            .then((serviceAccountToken) => {
              if (serviceAccountToken == null) {
                throw boom.unauthorized('missing service account authorization token');
              }
              return requestUserToken(serviceAccountToken);
            });
        }
        return cachedToken;
      });
  }

  return {
    getUserJwt: getUserJwt
  };
};
