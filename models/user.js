'use strict';

const bookshelf = require('bookshelf');

/**
 * User model
 *
 * @param {object} db database module
 * @returns {object} Bookshelf model
 */
module.exports = (db) => {
  if (db == null) {
    throw new Error('missing database object');
  }

  return bookshelf(db).Model.extend({
    tableName: 'user'
  });
};
