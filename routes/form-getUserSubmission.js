'use strict';

const Joi = require('joi');

/**
 * Get Newest Form Submission for user
 *
 * @param  {object} options - Route options with the keys: method, path, and handler
 * @return {object} The route
 */
module.exports = (options) => {
  if (options == null) {
    throw new Error('missing options object');
  }

  return {
    method: options.method,
    path: options.path,
    config: {
      validate: {
        headers: Joi.object({
          personid: Joi.string().required().description('person id'),
          clientid: Joi.number().integer().positive()
            .required().description('client id of the person')
        }).options({
          allowUnknown: true
        }),
        params: {
          formName: Joi.string().required().min(2).description('Form id from form.io')
        }
      },
      auth: {
        access: {
          scope: ['Get User Submission']
        }
      },
      description: 'Route to get newest form submission for a user by form id.',
      tags: ['formio', 'user', 'submission', 'api']
    },
    handler: options.handler
  };
};
