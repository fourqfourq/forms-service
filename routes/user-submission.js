'user strict';

const Joi = require('joi');

/**
 * Create new user
 *
 * @param {object} options - Route optons with the keys: method, path, and handler
 * @returns {object} The route
 */
module.exports = (options) => {
  if (options == null) {
    throw new Error('missing options object');
  }

  return {
    method: options.method,
    path: options.path,
    config: {
      validate: {
        headers: Joi.object({
          personid: Joi.string().guid().required().description('person id (guid)'),
          clientid: Joi.number().integer().positive()
            .required().description('client id of the person')
        }).options({
          allowUnknown: true
        }),
        params: Joi.object({
          formName: Joi.string().required()
            .description('form.io form name of the submission')
        }),
        payload: Joi.object().required().description('form.io submission data')
      },
      auth: {
        access: {
          scope: ['Submit Forms']
        }
      },
      description: 'Route to submit forms to form.io.',
      tags: ['formio', 'user', 'submission', 'jwt', 'api']
    },
    handler: options.handler
  };
};
