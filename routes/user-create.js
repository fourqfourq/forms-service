'user strict';

const Joi = require('joi');

/**
 * Create new user
 *
 * @param {object} options - Route optons with the keys: method, path, and handler
 * @returns {object} The route
 */
module.exports = (options) => {
  if (options == null) {
    throw new Error('missing options object');
  }

  return {
    method: options.method,
    path: options.path,
    config: {
      validate: {
        headers: Joi.object({
          personid: Joi.string().required().description('person id'),
          clientid: Joi.number().integer().positive()
            .required().description('client id of the person')
        }).options({
          allowUnknown: true
        })
      },
      auth: {
        access: {
          scope: ['Create Forms User']
        }
      },
      description: 'Route to create Form.io user and return access token.',
      tags: ['formio', 'user', 'create', 'jwt', 'token', 'api']
    },
    handler: options.handler
  };
};
