'use strict';

const Joi = require('joi');

/**
 * Get Forms by Tag
 *
 * @param  {object} options - Route options with the keys: method, path, and handler
 * @return {object} The route
 */
module.exports = (options) => {
  if (options == null) {
    throw new Error('missing options object');
  }

  return {
    method: options.method,
    path: options.path,
    config: {
      validate: {
        query: {
          tag: Joi.alternatives().try(Joi.array(), Joi.string())
            .required().description('Tag to lookup in form.io')
        }
      },
      auth: {
        access: {
          scope: ['Get Forms']
        }
      },
      description: 'Route to use form.io tags functionality to get forms by tag(s).',
      tags: ['formio', 'forms', 'api']
    },
    handler: options.handler
  };
};
