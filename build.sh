#!/bin/bash

rm -rf ./dist

mkdir -p ./dist/db/migration
mkdir -p ./dist/handlers
mkdir -p ./dist/log
mkdir -p ./dist/models
mkdir -p ./dist/routes

cp -R ./handlers/* ./dist/handlers
cp -R ./models/* ./dist/models
cp -R ./routes/* ./dist/routes
cp ./db/migration/* ./dist/db/migration

cp ./*.js ./dist
cp ./Dockerfile ./dist
cp ./package.json ./dist

cd ./dist
npm install --production
