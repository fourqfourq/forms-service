'use strict';

const redis = require('redis');
const bluebird = require('bluebird');

bluebird.promisifyAll(redis);

/**
 * Redis client
 *
 * @param {object} logger Logger module
 * @param {function} config configuration function
 * @returns {object} A Redis client
 * bluebird.promisifyAll(redis.RedisClient.prototype);
 * bluebird.promisifyAll(redis.Multi.prototype);
 */
module.exports = (logger, config) => {
  if (logger == null) {
    throw new Error('missing logger object');
  }

  if (config == null) {
    throw new Error('missing configuration object');
  }

  const host = config('REDIS_HOST');
  const port = config('REDIS_PORT');
  const password = config('REDIS_PASSWORD');
  const client = redis.createClient(port, host);

  if (password && typeof password === 'string') {
    client.auth(password);
  }

  client.on('error', (error) => {
    logger.error(`Redis error: ${error}`);
  });

  function ready() {
    return client.ready === true;
  }

  function failout() {
    return bluebird.try(() => null);
  }

  return {
    setReady: (value) => {
      client.ready = value;
    },
    expire: (key, seconds) => {
      return ready() ? client.EXPIREAsync(key, seconds) : failout();
    },
    delete: (key) => {
      return ready() ? client.DELAsync(key) : failout();
    },
    set: (key, value) => {
      return ready() ? client.setAsync(key, value) : failout();
    },
    get: (key) => {
      return ready() ? client.getAsync(key) : failout();
    },
    emit: (emitString, emitValue) => {
      if (client.ready) {
        client.emit(emitString, emitValue);
      }
    }
  };
};
