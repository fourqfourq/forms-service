'use strict';

const knex = require('knex');

module.exports = (config) => {
  const options = {
    client: config('DB_DIALECT'),
    connection: {
      port: config('DB_PORT'),
      host: config('DB_HOST'),
      db: config('DB_NAME'),
      user: config('DB_USER'),
      password: config('DB_PASSWORD')
    },
    debug: config('PRINT_SQL_STATEMENTS'),
    pool: {
      min: 2,
      max: 10
    }
  };

  return knex(options);
};
