/* eslint no-process-env: 0 */
'use strict';

const Code = require('code');
const Lab = require('lab');
const Logger = require('../logger');
const config = require('../config');
const mockery = require('mockery');

const lab = exports.lab = Lab.script();
const after = lab.after;
const before = lab.before;
const beforeEach = lab.beforeEach;
const describe = lab.describe;
const expect = Code.expect;
const it = lab.it;

const logger = Logger(config);

describe('Redis', () => {
  let Redis;

  before((done) => {
    mockery.registerAllowable('../redis');
    mockery.registerSubstitute('redis', 'redis-mock');
    mockery.enable({
      useCleanCache: true,
      warnOnReplace: false,
      warnOnUnregistered: false
    });
    done();
  });

  after((done) => {
    mockery.disable();
    mockery.deregisterAllowable('../redis');
    mockery.deregisterSubstitute('redis');
    done();
  });

  beforeEach((done) => {
    Redis = require('../redis');
    done();
  });

  it('returns a function', (done) => {
    expect(Redis).to.be.a.function();
    done();
  });

  it('throws error "missing logger object"', (done) => {
    expect(() => Redis(null, {})).to.throw('missing logger object');
    done();
  });

  it('throws error "missing configuration object"', (done) => {
    expect(() => Redis({}, null)).to.throw('missing configuration object');
    done();
  });

  it('connect to redis without a password', (done) => {
    const client = Redis(logger, config);
    expect(client).to.be.an.object();
    done();
  });

  it('redis sends an error event if ready', (done) => {
    const client = Redis(logger, config);
    client.setReady(true);
    client.emit('error', new Error('test'));

    // for now just asserting the client isn't null because there isn't anything
    //  terrific assertion against
    expect(client).to.not.be.null();
    done();
  });

  it('redis does not send error if not ready', (done) => {
    const client = Redis(logger, config);
    client.setReady(false);
    client.emit('error', new Error('test'));

    // for now just asserting the client isn't null because there isn't anything
    //  terrific assertion against
    expect(client).to.not.be.null();
    done();
  });

  it('connect to redis with a password', (done) => {
    process.env.REDIS_PASSWORD = 'password';
    const client = require('../redis')(logger, require('../config'));
    expect(client).to.be.an.object();
    done();
  });

  it('set & get', (done) => {
    const client = Redis(logger, config);
    const key = 'key';
    const value = 'value';

    client.setReady(true);
    client.set(key, value)
      .then((res) => {
        expect(res).equal('OK');
      })
      .then(() => client.get(key))
      .then((res) => expect(res).equal('value'))
      .then(() => done());
  });

  it('expire with ready client', (done) => {
    const cache = Redis(logger, config);
    const key = 'fookey';
    const seconds = 42;

    cache.setReady(true);
    cache.expire(key, seconds)
      .then((result) => {
        expect(result).to.not.be.null();
        done();
      });
  });

  it('expire without ready client', (done) => {
    const cache = Redis(logger, config);
    const key = 'fookey';
    const seconds = 42;

    cache.setReady(false);
    cache.expire(key, seconds)
      .then((result) => {
        expect(result).to.be.null();
        done();
      });
  });

  it('delete with ready client', (done) => {
    const cache = Redis(logger, config);
    const key = 'fookey';

    cache.setReady(true);
    cache.delete(key)
      .then((result) => {
        expect(result).to.not.be.null();
        done();
      });
  });

  it('delete without ready client', (done) => {
    const cache = Redis(logger, config);
    const key = 'fookey';

    cache.setReady(false);
    cache.delete(key)
      .then((result) => {
        expect(result).to.be.null();
        done();
      });
  });
});
