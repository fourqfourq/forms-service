'use strict';

const Code = require('code');
const Lab = require('lab');
const UserCreate = require('../../routes/user-create');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const expect = Code.expect;
const it = lab.it;

describe('Route - User Create', function() {
  it('should be a function', (done) => {
    expect(UserCreate).to.be.a.function();
    done();
  });

  it('throws error "missing options object"', (done) => {
    expect(() => UserCreate(null)).to.throw('missing options object');
    done();
  });

  it('should return the route object', (done) => {
    const options = {
      method: 'method',
      path: 'path',
      handler: () => 1
    };

    const userCreate = UserCreate(options);
    expect(userCreate.method).to.equal(options.method);
    expect(userCreate.path).to.equal(options.path);
    expect(userCreate.handler).to.equal(options.handler);
    done();
  });
});
