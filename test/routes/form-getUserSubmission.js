'use strict';

const code = require('code');
const Lab = require('lab');
const FormGetUserSubmission = require('../../routes/form-getUserSubmission');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const expect = code.expect;
const it = lab.it;

describe('Route - Forms Get User Submission', () => {
  it('should be a function', (done) => {
    expect(FormGetUserSubmission).to.be.a.function();
    done();
  });

  it('throws error "missing options object"', (done) => {
    expect(() => FormGetUserSubmission(null)).to.throw('missing options object');
    done();
  });

  it('should return the route object', (done) => {
    const options = {
      method: 'foo',
      path: '/bar',
      handler: () => 42
    };

    const formGetUserSubmission = FormGetUserSubmission(options);
    expect(formGetUserSubmission).to.be.an.object();
    expect(formGetUserSubmission.method).to.equal(options.method);
    expect(formGetUserSubmission.path).to.equal(options.path);
    expect(formGetUserSubmission.handler).to.equal(options.handler);
    done();
  });
});
