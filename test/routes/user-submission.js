'use strict';

const Code = require('code');
const Lab = require('lab');
const UserSubmission = require('../../routes/user-submission');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const expect = Code.expect;
const it = lab.it;

describe('Route - User Submission', function() {
  it('should be a function', (done) => {
    expect(UserSubmission).to.be.a.function();
    done();
  });

  it('throws error "missing options object"', (done) => {
    expect(() => UserSubmission(null)).to.throw('missing options object');
    done();
  });

  it('should return the route object', (done) => {
    const options = {
      method: 'method',
      path: 'path',
      handler: () => 1
    };

    const userSubmission = UserSubmission(options);
    expect(userSubmission.method).to.equal(options.method);
    expect(userSubmission.path).to.equal(options.path);
    expect(userSubmission.handler).to.equal(options.handler);
    done();
  });
});
