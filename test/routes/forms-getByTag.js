'use strict';

const code = require('code');
const Lab = require('lab');
const FormsGetByTag = require('../../routes/forms-getByTag');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const expect = code.expect;
const it = lab.it;

describe('Route - Forms Get by Tag', () => {
  it('should be a function', (done) => {
    expect(FormsGetByTag).to.be.a.function();
    done();
  });

  it('throws error "missing options object"', (done) => {
    expect(() => FormsGetByTag(null)).to.throw('missing options object');
    done();
  });

  it('should return the route object', (done) => {
    const options = {
      method: 'foo',
      path: '/bar',
      handler: () => 1
    };

    const formsGetByTag = FormsGetByTag(options);
    expect(formsGetByTag.method).to.equal(options.method);
    expect(formsGetByTag.path).to.equal(options.path);
    expect(formsGetByTag.handler).to.equal(options.handler);
    done();
  });
});
