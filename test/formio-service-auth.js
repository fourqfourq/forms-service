'use strict';

const FormioServiceAuth = require('../formio-service-auth');
const Code = require('code');
const Lab = require('lab');
const Logger = require('../logger');
const config = require('../config');
const mockery = require('mockery');
const nock = require('nock');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const after = lab.after;
const before = lab.before;
const beforeEach = lab.beforeEach;
const expect = Code.expect;
const it = lab.it;

describe('Formio Service Auth', () => {
  let formioServiceAuth;
  let logger;
  let cache;

  const formio = config('FORMIO_URL');
  const login = config('FORMIO_ROUTE_SERVICE_USER_LOGIN');
  const token = '0123456789';
  const serviceTokenCacheKey = config('CACHE_KEY_SERVICE_TOKEN');

  before((done) => {
    mockery.registerAllowable('../formio-service-auth');
    mockery.registerAllowable('../redis');
    mockery.registerSubstitute('redis', 'redis-mock');
    mockery.enable({
      useCleanCache: true,
      warnOnReplace: false,
      warnOnUnregistered: false
    });
    logger = Logger(config);
    cache = require('../redis')(logger, config);
    formioServiceAuth = FormioServiceAuth(cache, logger, config);
    done();
  });

  beforeEach((done) => {
    nock.cleanAll();
    done();
  });

  after((done) => {
    mockery.disable();
    mockery.deregisterAllowable('../formio-service-auth');
    mockery.deregisterAllowable('../redis');
    mockery.deregisterSubstitute('redis');
    done();
  });

  it('returns a function', (done) => {
    expect(FormioServiceAuth).to.be.a.function();
    done();
  });

  it('throws error "missing cache object"', (done) => {
    expect(() => FormioServiceAuth(null, {}, {})).to.throw('missing cache object');
    done();
  });

  it('throws error "missing logger object"', (done) => {
    expect(() => FormioServiceAuth({}, null, {})).to.throw('missing logger object');
    done();
  });

  it('throws error "missing configuration object"', (done) => {
    expect(() => FormioServiceAuth({}, {}, null)).to.throw('missing configuration object');
    done();
  });

  it('returns a jwt token', (done) => {
    nock(formio)
      .post(login)
      .reply(200, {}, { 'x-jwt-token': token });

    formioServiceAuth.getServiceUserToken()
      .then((response) => {
        expect(response).to.equal(token);
        done();
      });
  });

  it('returns a jwt token from cache', (done) => {
    nock(formio)
      .post(login)
      .reply(200, {}, { 'x-jwt-token': token });

    function getServiceToken() {
      formioServiceAuth.getServiceUserToken()
        .then((response) => {
          expect(response).to.equal(token);
          cache.delete(serviceTokenCacheKey);
          done();
        });
    }

    cache.setReady(true);
    cache.set(serviceTokenCacheKey, token)
      .then(getServiceToken);
  });

  it('returns an error', (done) => {
    formioServiceAuth = FormioServiceAuth(cache, logger, (value) => value);

    formioServiceAuth.getServiceUserToken()
      .catch((error) => {
        expect(error.code).to.equal('ENOTFOUND');
        done();
      });
  });
});
