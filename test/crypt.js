/* eslint no-plusplus: 0 */
'use strict';

const Code = require('code');
const config = require('../config');
const Crypt = require('../crypt');
const Lab = require('lab');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const expect = Code.expect;
const it = lab.it;

const crypt = Crypt(config);

describe('Crypt', () => {
  it('return a function', (done) => {
    expect(Crypt).to.be.a.function();
    done();
  });

  it('throws error "missing user object"', (done) => {
    expect(() => Crypt(null)).to.throw('missing configuration object');
    done();
  });

  it('encrypt / decrypt character codes', (done) => {
    let values;
    let encrypted;
    let decrypted;

    for (let num = 0; num <= 5000; num++) {
      values = values + String.fromCharCode(num);
      encrypted = crypt.encrypt(values);
      decrypted = crypt.decrypt(encrypted);
      expect(encrypted).to.not.equal(values);
      expect(decrypted).to.be.equal(values);
    }

    values.split('').forEach(function(value) {
      encrypted = crypt.encrypt(value);
      decrypted = crypt.decrypt(encrypted);
      expect(encrypted).to.not.equal(value);
      expect(decrypted).to.be.equal(value);
    });
    done();
  });

  it('encrypt / decrypt empty string', (done) => {
    const value = '';
    const encrypted = crypt.encrypt(value);
    const decrypted = crypt.decrypt(encrypted);

    expect(encrypted).to.equal(value);
    expect(decrypted).to.be.equal(value);
    done();
  });

  it('fails when value not a string or buffer', (done) => {
    const values = [1, null];

    values.forEach(function(value) {
      try {
        crypt.encrypt(value);
      } catch (ex) {
        expect(ex.message).to.equal('Not a string or buffer');
      }
    });
    done();
  });
});
