'use strict';

const FormioServiceAuth = require('../formio-service-auth');
const FormioUserAuth = require('../formio-user-auth');
const Code = require('code');
const Lab = require('lab');
const Logger = require('../logger');
const config = require('../config');
const mockKnex = require('mock-knex');
const mockery = require('mockery');
const nock = require('nock');
const uuid = require('node-uuid');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const after = lab.after;
const afterEach = lab.afterEach;
const before = lab.before;
const beforeEach = lab.beforeEach;
const expect = Code.expect;
const it = lab.it;

describe('Formio User Auth', () => {
  let cache;
  let db;
  let formioServiceAuth;
  let formioUserAuth;
  let logger;
  let tracker;
  let user;

  const clientId = 1;
  const formioProjectUri = config('FORMIO_PROJECT_URL');
  const formioUserSubmissionUri = config('FORMIO_ROUTE_USER_SUBMISSION');
  const formioUri = config('FORMIO_URL');
  const personId = uuid.v4();
  const cacheKey = `${clientId}_${personId}`;
  const serviceLoginUri = config('FORMIO_ROUTE_SERVICE_USER_LOGIN');
  const serviceAuthToken = '0123456789';
  const serviceTokenCacheKey = config('CACHE_KEY_SERVICE_TOKEN');
  const userAuthToken = 'foobar';

  before((done) => {
    tracker = mockKnex.getTracker();
    db = require('../database')(config);
    mockKnex.mock(db);
    user = require('../models/user')(db);
    logger = Logger(config);
    mockery.registerAllowable('../formio-user-auth');
    mockery.registerAllowable('../redis');
    mockery.registerSubstitute('redis', 'redis-mock');
    mockery.enable({
      useCleanCache: true,
      warnOnReplace: false,
      warnOnUnregistered: false
    });
    cache = require('../redis')(logger, config);
    formioServiceAuth = FormioServiceAuth(cache, logger, config);
    formioUserAuth = FormioUserAuth(formioServiceAuth, logger, config, user, cache);
    done();
  });

  beforeEach((done) => {
    tracker.install();
    nock.cleanAll();
    done();
  });

  after((done) => {
    mockery.disable();
    mockery.deregisterAllowable('../formio-user-auth');
    mockery.deregisterAllowable('../redis');
    mockery.deregisterSubstitute('redis');
    done();
  });

  afterEach((done) => {
    tracker.uninstall();
    cache.setReady(false);
    cache.delete(cacheKey);
    cache.delete(serviceTokenCacheKey);
    done();
  });

  it('return a function', (done) => {
    expect(FormioUserAuth).to.be.a.function();
    done();
  });

  it('throws error for missing formioServiceAuth object', (done) => {
    expect(() => FormioUserAuth(null, {}, {}, {}, {})).to.throw('missing formioServiceAuth object');
    done();
  });

  it('throws error for missing logger object', (done) => {
    expect(() => FormioUserAuth({}, null, {}, {}, {})).to.throw('missing logger object');
    done();
  });

  it('throws error for missing configuration object', (done) => {
    expect(() => FormioUserAuth({}, {}, null, {}, {})).to.throw('missing configuration object');
    done();
  });

  it('throws error for missing user object', (done) => {
    expect(() => FormioUserAuth({}, {}, {}, null, {})).to.throw('missing user object');
    done();
  });

  it('throws error for missing cache object', (done) => {
    expect(() => FormioUserAuth({}, {}, {}, {}, null)).to.throw('missing cache object');
    done();
  });

  it('returns a object containing the "getUserJwt" function', (done) => {
    expect(formioUserAuth).to.be.object();
    expect(formioUserAuth.getUserJwt).to.be.a.function();
    done();
  });

  it('returns a user token', (done) => {
    const formioUserId = '100';

    const headers = {
      'x-jwt-token': userAuthToken,
      'content-type': 'application/json; charset=utf-8'
    };

    const body = { '_id': '57a4ef9b5b7a477b00270ce9' };

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response([{
          personId: personId,
          formioUserId: formioUserId,
          clientId: clientId
        }]);
      }
    });

    nock(formioUri)
      .post(serviceLoginUri)
      .reply(200, {}, {
        'x-jwt-token': serviceAuthToken
      });

    nock(formioProjectUri)
      .get(`${formioUserSubmissionUri}/${formioUserId}`)
      .reply(201, body, headers);

    formioUserAuth.getUserJwt(personId, clientId)
      .then((jwt) => {
        expect(jwt).to.equal(userAuthToken);
        cache.delete(cacheKey);
        done();
      });
  });

  it('returns a user token from cache', (done) => {
    function getUserJwt() {
      formioUserAuth.getUserJwt(personId, clientId)
        .then((jwt) => {
          expect(jwt).to.equal(userAuthToken);
          cache.delete(cacheKey);
          done();
        });
    }
    cache.setReady(true);
    cache.set(cacheKey, userAuthToken).then(getUserJwt);
  });

  it('throws error if database model is null', (done) => {
    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response(null);
      }
    });

    nock(formioUri)
      .post(serviceLoginUri)
      .reply(200, {}, {
        'x-jwt-token': serviceAuthToken
      });

    formioUserAuth.getUserJwt(personId, clientId)
      .catch((error) => {
        expect(error).to.be.an.object();
        expect(error.message).to.equal('user has not been created in form.io');
        done();
      });
  });

  it('throws error if form.io user id is null', (done) => {
    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response([{
          personId: personId,
          formioUserId: null,
          clientId: clientId
        }]);
      }
    });

    nock(formioUri)
      .post(serviceLoginUri)
      .reply(200, {}, {
        'x-jwt-token': serviceAuthToken
      });

    formioUserAuth.getUserJwt(personId, clientId)
      .catch((error) => {
        expect(error).to.be.an.object();
        expect(error.message).to.equal('user has not been created in form.io');
        done();
      });
  });

  it('throws error if form.io user token is null', (done) => {
    const formioUserId = '100';

    const headers = {
      'x-jwt-token': null,
      'content-type': 'application/json; charset=utf-8'
    };

    const body = { '_id': '57a4ef9b5b7a477b00270ce9' };

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response([{
          personId: personId,
          formioUserId: formioUserId,
          clientId: clientId
        }]);
      }
    });

    nock(formioUri)
      .post(serviceLoginUri)
      .reply(200, {}, {
        'x-jwt-token': serviceAuthToken
      });

    nock(formioProjectUri)
      .get(`${formioUserSubmissionUri}/${formioUserId}`)
      .reply(201, body, headers);

    formioUserAuth.getUserJwt(personId, clientId)
      .catch((error) => {
        expect(error).to.be.an.object();
        expect(error.message).to.equal('failed to get user token from form.io');
        done();
      });
  });

  it('throws error if form.io service account token is null', (done) => {
    nock(formioUri)
      .post(serviceLoginUri)
      .reply(200, {}, {
        'x-jwt-token': null
      });

    formioUserAuth.getUserJwt(personId, clientId)
      .catch((error) => {
        expect(error).to.be.an.object();
        expect(error.message).to.equal('missing service account authorization token');
        done();
      });
  });
});
