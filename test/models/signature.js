'use strict';

const Code = require('code');
const Database = require('../../database');
const Lab = require('lab');
const SignatureModel = require('../../models/signature');
const config = require('../../config');
const mockDb = require('mock-knex');

const lab = exports.lab = Lab.script();
const after = lab.after;
const before = lab.before;
const describe = lab.describe;
const expect = Code.expect;
const it = lab.it;

const database = Database(config);

describe('Model - Signature', function() {
  before((done) => {
    mockDb.mock(database);
    done();
  });

  after((done) => {
    mockDb.unmock(database);
    done();
  });

  it('should be a function', (done) => {
    expect(SignatureModel).to.be.a.function();
    done();
  });

  it('throws error "missing database object"', (done) => {
    expect(() => SignatureModel(null)).to.throw('missing database object');
    done();
  });

  it('should return and object', (done) => {
    expect(SignatureModel(database)).to.be.a.function();
    done();
  });
});
