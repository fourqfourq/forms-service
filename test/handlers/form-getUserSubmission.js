/* eslint max-len:0 */
'use strict';

const Auth = require('../../formio-auth');
const FormioServiceAuth = require('../../formio-service-auth');
const FormioUserAuth = require('../../formio-user-auth');
const FormsGetUserSubmissionRoute = require('../../routes/form-getUserSubmission');
const FormsGetUserSubmissionHandler = require('../../handlers/form-getUserSubmission');
const Lab = require('lab');
const Logger = require('../../logger');
const Server = require('../../server');
const code = require('code');
const config = require('../../config');
const mockKnex = require('mock-knex');
const mockery = require('mockery');
const nock = require('nock');
const pack = require('../../package.json');
const uuid = require('node-uuid');

const lab = exports.lab = Lab.script();
const after = lab.after;
const afterEach = lab.afterEach;
const before = lab.before;
const beforeEach = lab.beforeEach;
const describe = lab.describe;
const expect = code.expect;
const it = lab.it;

describe('Handler - Forms Get User Submission', () => {
  let logger;
  let auth;
  let db;
  let formioServiceAuth;
  let formioUserAuth;
  let formsGetUserSubmissionHandler;
  let routes;
  let server;
  let cache;
  let tracker;
  let user;

  const clientId = 1;
  const formioUri = config('FORMIO_URL');
  const formioUserId = '88';
  const formName = 'foobar';
  const personId = uuid.v4();
  const projectUri = config('FORMIO_PROJECT_URL');
  const serviceAuthToken = '012345';
  const serviceLoginUri = config('FORMIO_ROUTE_SERVICE_USER_LOGIN');
  const serviceTokenCacheKey = config('CACHE_KEY_SERVICE_TOKEN');
  const userAuthToken = '42';
  const userSubmissionUri = config('FORMIO_ROUTE_USER_SUBMISSION');
  const formioSubmissionUri = config('FORMIO_ROUTE_FORM_GET_SUBMISSION');

  before((done) => {
    tracker = mockKnex.getTracker();
    db = require('../../database')(config);
    mockKnex.mock(db);
    user = require('../../models/user')(db);
    logger = Logger(config);

    mockery.registerAllowable('../../handlers/form-getUserSubmission');
    mockery.registerAllowable('../../redis');
    mockery.registerSubstitute('redis', 'redis-mock');
    mockery.enable({
      useCleanCache: true,
      warnOnReplace: false,
      warnOnUnregistered: false
    });

    cache = require('../../redis')(logger, config);
    formioServiceAuth = FormioServiceAuth(cache, logger, config);
    formioUserAuth = FormioUserAuth(formioServiceAuth, logger, config, user, cache);
    auth = Auth(formioServiceAuth, formioUserAuth);
    formsGetUserSubmissionHandler = FormsGetUserSubmissionHandler(auth, logger, config);
    routes = [
      FormsGetUserSubmissionRoute({
        path: '/form/{formName}/submission',
        method: 'GET',
        handler: formsGetUserSubmissionHandler
      })
    ];
    server = Server(pack, routes, logger, config);
    server.start(() => done());
  });

  beforeEach((done) => {
    tracker.install();
    nock.cleanAll();
    done();
  });

  after((done) => {
    mockery.disable();
    mockery.deregisterAllowable('../../handlers/form-getUserSubmission');
    mockery.deregisterAllowable('../../redis');
    mockery.deregisterSubstitute('redis');
    server.stop(() => done());
  });

  afterEach((done) => {
    tracker.uninstall();
    cache.delete(`${clientId}_${personId}`);
    cache.delete(serviceTokenCacheKey);
    done();
  });

  it('return a function', (done) => {
    expect(FormsGetUserSubmissionHandler).to.be.a.function();
    done();
  });

  it('throws error "missing authorization object"', (done) => {
    expect(() => FormsGetUserSubmissionHandler(null, {}, {}))
      .to.throw('missing authorization object');
    done();
  });

  it('throws error "missing logger object"', (done) => {
    expect(() => FormsGetUserSubmissionHandler({}, null, {}))
      .to.throw('missing logger object');
    done();
  });

  it('throws error "missing configuration object"', (done) => {
    expect(() => FormsGetUserSubmissionHandler({}, {}, null))
      .to.throw('missing configuration object');
    done();
  });

  it('returns a function', (done) => {
    expect(FormsGetUserSubmissionHandler({}, {}, {})).to.be.a.function();
    done();
  });

  it('GET /form/{formName}/submission return bad request if no person id', (done) => {
    const options = {
      method: 'GET',
      url: `/form/${formName}/submission`,
      headers: { clientid: 1 }
    };

    server.inject(options, (resp) => {
      expect(resp.statusCode).to.equal(400);
      expect(JSON.parse(resp.payload)).to.equal({
        statusCode: 400,
        error: 'Bad Request',
        message: 'child "personid" fails because ["personid" is required]',
        validation: {
          keys: [
            'personid'
          ],
          source: 'headers'
        }
      });
      done();
    });
  });

  it('GET /form/{formName}/submission return bad request if no client id', (done) => {
    const options = {
      method: 'GET',
      url: `/form/${formName}/submission`,
      headers: { personid: 'foobar42' }
    };

    server.inject(options, (resp) => {
      expect(resp.statusCode).to.equal(400);
      expect(JSON.parse(resp.payload)).to.equal({
        statusCode: 400,
        error: 'Bad Request',
        message: 'child "clientid" fails because ["clientid" is required]',
        validation: {
          keys: [
            'clientid'
          ],
          source: 'headers'
        }
      });
      done();
    });
  });

  it('GET /form/{formName}/submission return bad request if client id is not a number', (done) => {
    const options = {
      method: 'GET',
      url: `/form/${formName}/submission`,
      headers: {
        personid: 'foobar42',
        clientid: 'adsfkl'
      }
    };

    server.inject(options, (resp) => {
      expect(resp.statusCode).to.equal(400);
      expect(JSON.parse(resp.payload)).to.equal({
        statusCode: 400,
        error: 'Bad Request',
        message: 'child "clientid" fails because ["clientid" must be a number]',
        validation: {
          keys: [
            'clientid'
          ],
          source: 'headers'
        }
      });
      done();
    });
  });

  it('GET /form/{formName}/submission return 200 for pre-existing user submission', (done) => {
    const options = {
      method: 'GET',
      url: `/form/${formName}/submission`,
      headers: {
        personid: personId,
        clientid: clientId
      }
    };

    const userAuthHeaders = {
      'x-jwt-token': userAuthToken,
      'content-type': 'application/json; charset=utf-8'
    };

    const userAuthBody = { '_id': '57a4ef9b5b7a477b00270ce9' };

    const submissionResponseBody = [
      {
        '_id': '57b7993fb362857e0033649e',
        'modified': '2016-08-19T23:41:51.950Z',
        'data': {},
        'form': '578d3d569c27b167000030c5',
        'created': '2016-08-19T23:41:51.941Z',
        'externalIds': [],
        'access': [],
        'roles': [],
        'owner': '5788e1e99f13df670029f405'
      }
    ];

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response([{
          personId: personId,
          formioUserId: formioUserId,
          clientId: clientId
        }]);
      }
    });

    nock(formioUri)
      .post(serviceLoginUri)
      .reply(201, {}, {
        'x-jwt-token': serviceAuthToken
      });

    nock(projectUri)
      .get(`${userSubmissionUri}/${formioUserId}`)
      .reply(201, userAuthBody, userAuthHeaders);

    nock(projectUri)
      .get(`/${formName}${formioSubmissionUri}?sort=-modified&limit=1`)
      .reply(200, submissionResponseBody, {});

    server.inject(options, (resp) => {
      expect(resp.statusCode).to.equal(200);
      expect(JSON.parse(resp.payload)).to.equal(submissionResponseBody);
      done();
    });
  });

  it('GET /form/{formName}/submission return unauthorized if user cannot be authenticated', (done) => {
    const options = {
      method: 'GET',
      url: `/form/${formName}/submission`,
      headers: {
        personid: personId,
        clientid: clientId
      }
    };

    const headers = {
      'content-type': 'application/json; charset=utf-8'
    };

    const body = { '_id': '57a4ef9b5b7a477b00270ce9' };

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response([{
          personId: personId,
          formioUserId: formioUserId,
          clientId: clientId
        }]);
      }
    });

    nock(formioUri)
      .post(serviceLoginUri)
      .reply(201, {}, {
        'x-jwt-token': serviceAuthToken
      });

    nock(projectUri)
      .get(`${userSubmissionUri}/${formioUserId}`)
      .reply(201, body, headers);

    server.inject(options, (resp) => {
      expect(resp.statusCode).to.equal(401);
      expect(JSON.parse(resp.payload)).to.equal({
        statusCode: 401,
        error: 'Unauthorized',
        message: 'failed to get user token from form.io'
      });
      done();
    });
  });

  it('GET /form/{formName}/submission return unauthorized if service account cannot be authenticated', (done) => {
    const options = {
      method: 'GET',
      url: `/form/${formName}/submission`,
      headers: {
        personid: personId,
        clientid: clientId
      }
    };

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response(formioUserId);
      }
    });

    nock(formioUri)
      .post(serviceLoginUri)
      .reply(201, {}, {
        'x-jwt-token': null
      });

    server.inject(options, (resp) => {
      expect(resp.statusCode).to.equal(401);
      expect(JSON.parse(resp.payload)).to.equal({
        statusCode: 401,
        error: 'Unauthorized',
        message: 'missing service account authorization token'
      });
      done();
    });
  });

  it('GET /form/{formName}/submission return unauthorized if user cannot be authenticated', (done) => {
    const options = {
      method: 'GET',
      url: `/form/${formName}/submission`,
      headers: {
        personid: personId,
        clientid: clientId
      }
    };

    const headers = {
      'content-type': 'application/json; charset=utf-8'
    };

    const body = { '_id': '57a4ef9b5b7a477b00270ce9' };

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response([{
          personId: personId,
          formioUserId: formioUserId,
          clientId: clientId
        }]);
      }
    });

    nock(formioUri)
      .post(serviceLoginUri)
      .reply(201, {}, {
        'x-jwt-token': serviceAuthToken
      });

    nock(projectUri)
      .get(`${userSubmissionUri}/${formioUserId}`)
      .reply(201, body, headers);

    server.inject(options, (resp) => {
      expect(resp.statusCode).to.equal(401);
      expect(JSON.parse(resp.payload)).to.equal({
        statusCode: 401,
        error: 'Unauthorized',
        message: 'failed to get user token from form.io'
      });
      done();
    });
  });
});
