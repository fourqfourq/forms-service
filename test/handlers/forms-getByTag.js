/* eslint max-len:0 */
'use strict';

const Auth = require('../../formio-auth');
const FormsGetByTagHandler = require('../../handlers/forms-getByTag');
const FormsGetByTagRoute = require('../../routes/forms-getByTag');
const FormioServiceAuth = require('../../formio-service-auth');
const FormioUserAuth = require('../../formio-user-auth');
const Lab = require('lab');
const Logger = require('../../logger');
const Server = require('../../server');
const code = require('code');
const config = require('../../config');
const formsFixture = require('../fixtures/formsByTag');
const mockKnex = require('mock-knex');
const mockery = require('mockery');
const nock = require('nock');
const pack = require('../../package.json');

const lab = exports.lab = Lab.script();
const after = lab.after;
const afterEach = lab.afterEach;
const before = lab.before;
const beforeEach = lab.beforeEach;
const describe = lab.describe;
const expect = code.expect;
const it = lab.it;

// tests
describe('Handler - Forms Get By Tag', () => {
  let logger;
  let auth;
  let db;
  let formsGetByTagHandler;
  let formioServiceAuth;
  let formioUserAuth;
  let routes;
  let server;
  let cache;
  let tracker;
  let user;

  const formioUrl = config('FORMIO_URL');
  const projectUrl = config('FORMIO_PROJECT_URL');
  const formsGet = config('FORMIO_ROUTE_FORM_GET');
  const serviceLogin = config('FORMIO_ROUTE_SERVICE_USER_LOGIN');
  const serviceTokenCacheKey = config('CACHE_KEY_SERVICE_TOKEN');
  const tag = 'foobar';
  const token = '0123456789';

  before((done) => {
    tracker = mockKnex.getTracker();
    db = require('../../database')(config);
    mockKnex.mock(db);
    user = require('../../models/user')(db);
    logger = Logger(config);

    mockery.registerAllowable('../../handlers/forms-getByTag');
    mockery.registerAllowable('../../redis');
    mockery.registerSubstitute('redis', 'redis-mock');
    mockery.enable({
      useCleanCache: true,
      warnOnReplace: false,
      warnOnUnregistered: false
    });
    cache = require('../../redis')(logger, config);
    formioServiceAuth = FormioServiceAuth(cache, logger, config);
    formioUserAuth = FormioUserAuth(formioServiceAuth, logger, config, user, cache);
    auth = Auth(formioServiceAuth, formioUserAuth);
    formsGetByTagHandler = FormsGetByTagHandler(auth, logger, config);
    routes = [
      FormsGetByTagRoute({ path: '/forms', method: 'GET', handler: formsGetByTagHandler })
    ];
    server = Server(pack, routes, logger, config);
    server.start(() => done());
  });

  beforeEach((done) => {
    tracker.install();
    nock.cleanAll();
    done();
  });

  after((done) => {
    mockery.disable();
    mockery.deregisterAllowable('../../handlers/forms-getByTag');
    mockery.deregisterAllowable('../../redis');
    mockery.deregisterSubstitute('redis');
    server.stop(() => done());
  });

  afterEach((done) => {
    tracker.uninstall();
    cache.delete(serviceTokenCacheKey);
    done();
  });

  it('return a function', (done) => {
    expect(FormsGetByTagHandler).to.be.a.function();
    done();
  });

  it('throws error "missing authorization object"', (done) => {
    expect(() => FormsGetByTagHandler(null, {}, {})).to.throw('missing authorization object');
    done();
  });

  it('throws error "missing logger object"', (done) => {
    expect(() => FormsGetByTagHandler({}, null, {})).to.throw('missing logger object');
    done();
  });

  it('throws error "missing configuration object"', (done) => {
    expect(() => FormsGetByTagHandler({}, {}, null)).to.throw('missing configuration object');
    done();
  });

  it('returns a function', (done) => {
    expect(FormsGetByTagHandler({}, {}, {})).to.be.a.function();
    done();
  });

  it('GET /forms should return 200 with array if multiple tags specified', (done) => {
    const tags = ['foobar', 'fubar'];

    const options = {
      method: 'GET',
      url: `/forms?tag=${tags[0]}&tag=${tags[1]}`
    };

    // service login
    nock(formioUrl)
      .post(serviceLogin)
      .reply(200, {}, { 'x-jwt-token': token });

    // forms retrieval
    nock(projectUrl)
      .get(formsGet)
      .query({
        tags: tags[0]
      })
      .reply(200, formsFixture);

    nock(projectUrl)
        .get(formsGet)
        .query({
          tags: tags[1]
        })
        .reply(200, formsFixture);

    const localFixture = formsFixture.slice(0);
    localFixture.splice(2, 1);

    server.inject(options, (resp) => {
      expect(resp.statusCode).to.equal(200);
      expect(JSON.parse(resp.payload)).to.equal(localFixture);
      done();
    });
  });

  it('GET /forms should return 200 with array if single tag specified', (done) => {
    const options = {
      method: 'GET',
      url: `/forms?tag=${tag}`
    };

    // service login
    nock(formioUrl)
      .post(serviceLogin)
      .reply(200, {}, { 'x-jwt-token': token });

    // forms retrieval
    nock(projectUrl)
      .get(formsGet)
      .query({
        tags: tag
      })
      .reply(200, formsFixture);

    const localFixture = formsFixture.slice(0);
    localFixture.splice(2, 1);

    server.inject(options, (resp) => {
      expect(resp.statusCode).to.equal(200);
      expect(JSON.parse(resp.payload)).to.equal(localFixture);
      done();
    });
  });

  it('GET /forms should return 200 with empty array if 404 - NOT FOUND returned', (done) => {
    const options = {
      method: 'GET',
      url: `/forms?tag=${tag}`
    };

    // service login
    nock(formioUrl)
      .post(serviceLogin)
      .reply(200, {}, { 'x-jwt-token': token });

    // forms retrieval
    nock(projectUrl)
      .get(formsGet)
      .query({
        tags: tag
      })
      .replyWithError({ 'statusCode': 404, 'error': 'Not Found' });

    server.inject(options, (resp) => {
      expect(resp.statusCode).to.equal(200);
      expect(JSON.parse(resp.payload)).to.equal([]);
      done();
    });
  });

  it('GET /forms should return 401 - Forbidden error', (done) => {
    const options = {
      method: 'GET',
      url: `/forms?tag=${tag}`
    };

    // service login
    nock(formioUrl)
      .post(serviceLogin)
      .reply(200, {}, { 'x-jwt-token': token });

    // forms retrieval
    nock(projectUrl)
      .get(formsGet)
      .query({
        tags: tag
      })
      .replyWithError({ 'statusCode': 401, 'message': 'Unauthorized' });

    server.inject(options, (resp) => {
      expect(resp.statusCode).to.equal(401);
      expect(JSON.parse(resp.payload)).to.equal({ statusCode: 401, error: 'Unauthorized', message: 'Unauthorized' });
      done();
    });
  });

  it('GET /forms should return 400 - Bad Request if tag retrieve fails', (done) => {
    const options = {
      method: 'GET',
      url: `/forms?tag=${tag}`
    };

    // service login
    nock(formioUrl)
      .post(serviceLogin)
      .reply(200, {}, { 'x-jwt-token': token });

    // forms retrieval
    nock(projectUrl)
      .get(formsGet)
      .query({
        tags: tag
      })
      .replyWithError({ 'statusCode': 400, 'error': 'Bad Request', 'message': 'I am the breaker of things' });

    server.inject(options, (resp) => {
      expect(resp.statusCode).to.equal(400);
      expect(JSON.parse(resp.payload)).to.equal({ statusCode: 400, error: 'Bad Request', message: 'I am the breaker of things' });
      done();
    });
  });

  it('GET /forms should return 400 - Bad Request if service auth fails', (done) => {
    const options = {
      method: 'GET',
      url: '/forms?tag=foobar'
    };

    nock(formioUrl)
      .post(serviceLogin)
      .replyWithError('boom');

    server.inject(options, (resp) => {
      expect(resp.statusCode).to.equal(400);
      expect(JSON.parse(resp.payload)).to.equal({ statusCode: 400, error: 'Bad Request', message: 'boom' });
      done();
    });
  });

  it('GET /forms should return 400 - Bad Request if no tag query parameters', (done) => {
    const options = {
      method: 'GET',
      url: '/forms'
    };

    server.inject(options, (resp) => {
      expect(resp.statusCode).to.equal(400);
      expect(resp.result).to.equal({
        error: 'Bad Request',
        message: 'child "tag" fails because ["tag" is required]',
        statusCode: 400,
        validation: {
          keys: ['tag'],
          source: 'query'
        }
      });
      done();
    });
  });

  it('GET /forms should return 400 - Bad Request if token is empty from form.io', (done) => {
    const localToken = null;

    const options = {
      method: 'GET',
      url: `/forms?tag=${tag}`
    };

    // service login
    nock(formioUrl)
      .post(serviceLogin)
      .reply(200, {}, { 'x-jwt-token': localToken });

    // forms retrieval
    nock(projectUrl)
      .get(formsGet)
      .query({
        tags: tag
      })
      .reply(200, formsFixture);

    server.inject(options, (resp) => {
      expect(resp.statusCode).to.equal(400);
      expect(JSON.parse(resp.payload)).to.equal({ statusCode: 400, error: 'Bad Request', message: 'missing authorization token' });
      done();
    });
  });
});
