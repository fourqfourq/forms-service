/* eslint max-len:0 */
'use strict';

const Auth = require('../../formio-auth');
const Code = require('code');
const FormioServiceAuth = require('../../formio-service-auth');
const FormioUserAuth = require('../../formio-user-auth');
const Lab = require('lab');
const Logger = require('../../logger');
const Server = require('../../server');
const UserSubmissionHandler = require('../../handlers/user-submission');
const UserSubmissionRoute = require('../../routes/user-submission');
const config = require('../../config');
const mockKnex = require('mock-knex');
const mockery = require('mockery');
const nock = require('nock');
const pack = require('../../package.json');
const uuid = require('node-uuid');

const lab = exports.lab = Lab.script();
const after = lab.after;
const afterEach = lab.afterEach;
const before = lab.before;
const beforeEach = lab.beforeEach;
const describe = lab.describe;
const expect = Code.expect;
const it = lab.it;

describe('Handler - User Submission', () => {
  let auth;
  let cache;
  let db;
  let formioServiceAuth;
  let formioUserAuth;
  let logger;
  let routes;
  let server;
  let tracker;
  let user;
  let userSubmissionHandler;

  const clientId = 1;
  const formioUrl = config('FORMIO_URL');
  const personId = uuid.v4();
  const cacheKey = `${clientId}_${personId}`;
  const projectUrl = config('FORMIO_PROJECT_URL');
  const serviceLogin = config('FORMIO_ROUTE_SERVICE_USER_LOGIN');
  const serviceTokenCacheKey = config('CACHE_KEY_SERVICE_TOKEN');
  const userSubmission = config('FORMIO_ROUTE_USER_SUBMISSION');
  const token = '0123456789';

  before((done) => {
    tracker = mockKnex.getTracker();
    db = require('../../database')(config);
    mockKnex.mock(db);
    user = require('../../models/user')(db);
    mockery.registerAllowable('../../handlers/user-submission');
    mockery.registerAllowable('../../redis');
    mockery.registerSubstitute('redis', 'redis-mock');
    mockery.enable({
      useCleanCache: true,
      warnOnReplace: false,
      warnOnUnregistered: false
    });
    logger = Logger(config);
    cache = require('../../redis')(logger, config);
    formioServiceAuth = FormioServiceAuth(cache, logger, config);
    formioUserAuth = FormioUserAuth(formioServiceAuth, logger, config, user, cache);
    auth = Auth(formioServiceAuth, formioUserAuth);
    userSubmissionHandler = UserSubmissionHandler(cache, user, auth, logger, config);
    routes = [
      UserSubmissionRoute({
        path: '/{formName}/submission',
        method: 'POST',
        handler: userSubmissionHandler
      })
    ];
    server = Server(pack, routes, logger, config);
    server.start(() => done());
  });

  beforeEach((done) => {
    tracker.install();
    nock.cleanAll();
    done();
  });

  after((done) => {
    mockery.disable();
    mockery.deregisterAllowable('../../handlers/user-submission');
    mockery.deregisterAllowable('../../redis');
    mockery.deregisterSubstitute('redis');
    server.stop(() => done());
  });

  afterEach((done) => {
    tracker.uninstall();
    cache.delete(cacheKey);
    cache.delete(serviceTokenCacheKey);
    done();
  });

  it('return a function', (done) => {
    expect(UserSubmissionHandler).to.be.a.function();
    done();
  });

  it('throws error "missing cache object"', (done) => {
    expect(() => UserSubmissionHandler(null, {}, {}, {}, {})).to.throw('missing cache object');
    done();
  });

  it('throws error "missing user object"', (done) => {
    expect(() => UserSubmissionHandler({}, null, {}, {}, {})).to.throw('missing user object');
    done();
  });

  it('throws error "missing authorization object"', (done) => {
    expect(() => UserSubmissionHandler({}, {}, null, {}, {})).to.throw('missing authorization object');
    done();
  });

  it('throws error "missing logger object"', (done) => {
    expect(() => UserSubmissionHandler({}, {}, {}, null, {})).to.throw('missing logger object');
    done();
  });

  it('throws error "missing configuration object"', (done) => {
    expect(() => UserSubmissionHandler({}, {}, {}, {}, null)).to.throw('missing configuration object');
    done();
  });

  it('returns a function', (done) => {
    expect(UserSubmissionHandler({}, {}, {}, {}, config)).to.be.a.function();
    done();
  });

  it('returns a 201', (done) => {
    const options = {
      method: 'POST',
      payload: { test: 'test' },
      url: '/testForm/submission',
      headers: {
        personid: personId,
        clientid: clientId
      }
    };

    const headers = {
      'x-jwt-token': token,
      'content-type': 'application/json; charset=utf-8'
    };

    const body = { '_id': '57a4ef9b5b7a477b00270ce9' };
    const formioUserId = 666;

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response([{
          personId: personId,
          clientId: clientId,
          formioUserId: formioUserId }]);
      }
    });

    nock(projectUrl)
      .get(`${userSubmission}/${formioUserId}`)
      .reply(201, body, headers);

    nock(formioUrl)
      .post(serviceLogin)
      .reply(200, {}, { 'x-jwt-token': token });

    server.inject(options, (res) => {
      expect(res.statusCode).to.equal(201);
      expect(res.payload).to.equal('');
      done();
    });
  });

  it('returns a ...', (done) => {
    const options = {
      method: 'POST',
      payload: { test: 'test' },
      url: '/testForm/submission',
      headers: {
        personid: personId,
        clientid: clientId
      }
    };

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response(null);
      }
    });

    server.inject(options, (res) => {
      expect(res.statusCode).to.equal(404);
      expect(JSON.parse(res.payload)).to.equal({ statusCode: 404, error: 'Not Found', message: 'User not found' });
      done();
    });
  });
});
