/* eslint max-len:0 */
'use strict';

const Auth = require('../../formio-auth');
const Code = require('code');
const Crypt = require('../../crypt');
const FormioServiceAuth = require('../../formio-service-auth');
const FormioUserAuth = require('../../formio-user-auth');
const Lab = require('lab');
const Logger = require('../../logger');
const Server = require('../../server');
const UserCreateHandler = require('../../handlers/user-create');
const UserCreateRoute = require('../../routes/user-create');
const config = require('../../config');
const mockKnex = require('mock-knex');
const mockery = require('mockery');
const nock = require('nock');
const pack = require('../../package.json');
const uuid = require('node-uuid');

const lab = exports.lab = Lab.script();
const after = lab.after;
const afterEach = lab.afterEach;
const before = lab.before;
const beforeEach = lab.beforeEach;
const describe = lab.describe;
const expect = Code.expect;
const it = lab.it;


describe('Handler - User Create', () => {
  let auth;
  let cache;
  let crypt;
  let db;
  let formioServiceAuth;
  let formioUserAuth;
  let logger;
  let routes;
  let server;
  let tracker;
  let user;
  let userCreateHandler;

  const clientId = 1;
  const formioUrl = config('FORMIO_URL');
  const personId = uuid.v4();
  const cacheKey = `${clientId}_${personId}`;
  const projectUrl = config('FORMIO_PROJECT_URL');
  const userSubmission = config('FORMIO_ROUTE_USER_SUBMISSION');
  const serviceLogin = config('FORMIO_ROUTE_SERVICE_USER_LOGIN');
  const serviceTokenCacheKey = config('CACHE_KEY_SERVICE_TOKEN');
  const token = '0123456789';

  before((done) => {
    tracker = mockKnex.getTracker();
    db = require('../../database')(config);
    mockKnex.mock(db);
    user = require('../../models/user')(db);
    logger = Logger(config);
    crypt = Crypt(config);
    mockery.registerAllowable('../../handlers/user-create');
    mockery.registerAllowable('../../redis');
    mockery.registerSubstitute('redis', 'redis-mock');
    mockery.enable({
      useCleanCache: true,
      warnOnReplace: false,
      warnOnUnregistered: false
    });
    cache = require('../../redis')(logger, config);
    formioServiceAuth = FormioServiceAuth(cache, logger, config);
    formioUserAuth = FormioUserAuth(formioServiceAuth, logger, config, user, cache);
    auth = Auth(formioServiceAuth, formioUserAuth);
    userCreateHandler = UserCreateHandler(cache, user, auth, logger, config, crypt);
    routes = [
      UserCreateRoute({
        path: '/user/create',
        method: 'POST',
        handler: userCreateHandler
      })
    ];
    server = Server(pack, routes, logger, config);
    server.start(() => done());
  });

  beforeEach((done) => {
    tracker.install();
    nock.cleanAll();
    done();
  });

  after((done) => {
    mockery.disable();
    mockery.deregisterAllowable('../../handlers/user-create');
    mockery.deregisterAllowable('../../redis');
    mockery.deregisterSubstitute('redis');
    server.stop(() => done());
  });

  afterEach((done) => {
    tracker.uninstall();
    cache.setReady(false);
    cache.delete(cacheKey);
    cache.delete(serviceTokenCacheKey);
    done();
  });

  it('return a function', (done) => {
    expect(UserCreateHandler).to.be.a.function();
    done();
  });

  it('throws error "missing cache object"', (done) => {
    expect(() => UserCreateHandler(null, {}, {}, {}, {}, {})).to.throw('missing cache object');
    done();
  });

  it('throws error "missing user object"', (done) => {
    expect(() => UserCreateHandler({}, null, {}, {}, {}, {})).to.throw('missing user object');
    done();
  });

  it('throws error "missing authorization object"', (done) => {
    expect(() => UserCreateHandler({}, {}, null, {}, {}, {})).to.throw('missing authorization object');
    done();
  });

  it('throws error "missing logger object"', (done) => {
    expect(() => UserCreateHandler({}, {}, {}, null, {}, {})).to.throw('missing logger object');
    done();
  });

  it('throws error "missing configuration object"', (done) => {
    expect(() => UserCreateHandler({}, {}, {}, {}, null, {})).to.throw('missing configuration object');
    done();
  });

  it('throws error "missing crypt object"', (done) => {
    expect(() => UserCreateHandler({}, {}, {}, {}, {}, null)).to.throw('missing crypt object');
    done();
  });

  it('returns a function', (done) => {
    expect(UserCreateHandler({}, {}, {}, {}, config, {})).to.be.a.function();
    done();
  });

  it('returns a JWT token with status code 201', (done) => {
    const options = {
      method: 'POST',
      url: '/user/create',
      headers: {
        personid: personId,
        clientid: clientId
      }
    };

    const headers = {
      'x-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjp7Il9pZCI6IjU3ODhlMWU5OWYxM2RmNjcwMDI5ZjQwNSJ9LCJmb3JtIjp7Il9pZCI6IjU1M2RiOTRlNzJmNzAyZTcxNGRkOTc3OSIsInByb2plY3QiOiI1NTNkYjkyZjcyZjcwMmU3MTRkZDk3NzgifSwiaWF0IjoxNDcwNDI2OTg1LCJleHAiOjE1MDY3MTQ5ODV9.79bTA54oqHsqdUUbzgqcy9tVwNOSWat14OiJvAKRvsQ',
      'content-type': 'application/json; charset=utf-8'
    };

    const body = {
      modified: '2016-08-05T19:57:15.626Z',
      data: {
        email: 'test@email.com'
      },
      form: '578d312e9c27b1670000306f',
      created: '2016-08-05T19:57:15.607Z',
      '_id': '57a4ef9b5b7a477b00270ce9',
      externalIds: [],
      access: [],
      roles: [
        '578d312e9c27b1670000306d'
      ],
      owner: '5788e1e99f13df670029f405'
    };

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response(null);
      }

      if (query.method === 'insert') {
        query.response([{}]);
      }
    });

    // service user login
    nock(formioUrl)
      .post(serviceLogin)
      .reply(200, {}, {
        'x-jwt-token': token
      });

    // user create
    nock(projectUrl)
      .post(userSubmission)
      .reply(201, body, headers);

    server.inject(options, (res) => {
      expect(res.statusCode).to.equal(201);
      expect(res.payload).to.equal('');
      expect(res.headers['x-jwt-token']).to.equal(headers['x-jwt-token']);
      done();
    });
  });

  it('returns a JWT token with status code 200', (done) => {
    const formioUserId = '100';
    const options = {
      method: 'POST',
      url: '/user/create',
      headers: {
        personid: personId,
        clientid: clientId
      }
    };

    const headers = {
      'x-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjp7Il9pZCI6IjU3ODhlMWU5OWYxM2RmNjcwMDI5ZjQwNSJ9LCJmb3JtIjp7Il9pZCI6IjU1M2RiOTRlNzJmNzAyZTcxNGRkOTc3OSIsInByb2plY3QiOiI1NTNkYjkyZjcyZjcwMmU3MTRkZDk3NzgifSwiaWF0IjoxNDcwNDI2OTg1LCJleHAiOjE1MDY3MTQ5ODV9.79bTA54oqHsqdUUbzgqcy9tVwNOSWat14OiJvAKRvsQ',
      'content-type': 'application/json; charset=utf-8'
    };

    const body = { '_id': '57a4ef9b5b7a477b00270ce9' };

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response([{
          personId: personId,
          formioUserId: formioUserId,
          clientId: clientId
        }]);
      }
    });

    // service user login
    nock(formioUrl)
      .post(serviceLogin)
      .reply(200, {}, {
        'x-jwt-token': token
      });

    // get user
    nock(projectUrl)
      .get(`${userSubmission}/${formioUserId}`)
      .reply(201, body, headers);

    server.inject(options, (res) => {
      expect(res.statusCode).to.equal(200);
      expect(res.payload).to.equal('');
      expect(res.headers['x-jwt-token']).to.equal(headers['x-jwt-token']);
      done();
    });
  });

  it('returns a JWT token with status code 200 from cache', (done) => {
    const options = {
      method: 'POST',
      url: '/user/create',
      headers: {
        personid: personId,
        clientid: clientId
      }
    };

    function inject() {
      server.inject(options, (res) => {
        expect(res.statusCode).to.equal(200);
        expect(res.payload).to.equal('');
        expect(res.headers['x-jwt-token']).to.equal(token);
        done();
      });
    }

    cache.setReady(true);
    cache.set(cacheKey, token).then(inject);
  });

  it('returns forbidden error', (done) => {
    const options = {
      method: 'POST',
      url: '/user/create',
      headers: {
        personid: personId,
        clientid: clientId
      }
    };

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response(null);
      }
    });

    // service user login
    nock(formioUrl)
      .post(serviceLogin)
      .reply(200, {}, {
        'x-jwt-token': token
      });

    // user create
    nock(projectUrl)
      .post(userSubmission)
      .reply(401);

    server.inject(options, (res) => {
      expect(res.statusCode).to.equal(401);
      expect(JSON.parse(res.payload)).to.equal({
        statusCode: 401,
        error: 'Unauthorized',
        message: 'Unauthorized'
      });
      done();
    });
  });

  it('returns bad request', (done) => {
    const options = {
      method: 'POST',
      url: '/user/create',
      headers: {
        personid: personId,
        clientid: clientId
      }
    };

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response(null);
      }
    });

    nock(formioUrl)
      .post(serviceLogin)
      .replyWithError('boom');

    server.inject(options, (res) => {
      expect(res.statusCode).to.equal(500);
      expect(JSON.parse(res.payload)).to.equal({
        statusCode: 500,
        error: 'Internal Server Error',
        message: 'An internal server error occurred'
      });
      done();
    });
  });

  it('returns bad request', (done) => {
    const options = {
      method: 'POST',
      url: '/user/create',
      headers: {
        personid: personId,
        clientid: clientId
      }
    };

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response(null);
      }
    });

    nock(formioUrl)
      .post(serviceLogin)
      .reply(201, {}, {
        'x-jwt-token': token
      });

    nock(projectUrl)
      .post(userSubmission)
      .reply(400);

    server.inject(options, (res) => {
      expect(res.statusCode).to.equal(400);
      expect(JSON.parse(res.payload)).to.equal({
        statusCode: 400,
        error: 'Bad Request',
        message: 'Bad Request'
      });
      done();
    });
  });
});
