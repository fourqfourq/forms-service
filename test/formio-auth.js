'use strict';

const Auth = require('../formio-auth');
const Code = require('code');

const FormioServiceAuth = require('../formio-service-auth');
const FormioUserAuth = require('../formio-user-auth');
const Lab = require('lab');
const Logger = require('../logger');
const config = require('../config');
const mockKnex = require('mock-knex');
const mockery = require('mockery');
const nock = require('nock');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const after = lab.after;
const afterEach = lab.afterEach;
const before = lab.before;
const beforeEach = lab.beforeEach;
const expect = Code.expect;
const it = lab.it;

describe('Auth', () => {
  let auth;
  let cache;
  let db;
  let formioServiceAuth;
  let formioUserAuth;
  let logger;
  let tracker;
  let user;

  before((done) => {
    tracker = mockKnex.getTracker();
    db = require('../database')(config);
    mockKnex.mock(db);
    user = require('../models/user')(db);
    logger = Logger(config);
    cache = require('../redis')(logger, config);

    mockery.registerAllowable('../formio-auth');
    mockery.registerAllowable('../redis');
    mockery.registerSubstitute('redis', 'redis-mock');
    mockery.enable({
      useCleanCache: true,
      warnOnReplace: false,
      warnOnUnregistered: false
    });

    formioServiceAuth = FormioServiceAuth(cache, logger, config);
    formioUserAuth = FormioUserAuth(formioServiceAuth, logger, config, user, cache);
    auth = Auth(formioServiceAuth, formioUserAuth);
    done();
  });

  beforeEach((done) => {
    tracker.install();
    nock.cleanAll();
    done();
  });

  after((done) => {
    mockery.disable();
    mockery.deregisterAllowable('../formio-auth');
    mockery.deregisterAllowable('../redis');
    mockery.deregisterSubstitute('redis');
    done();
  });

  afterEach((done) => {
    tracker.uninstall();
    done();
  });

  it('returns a function', (done) => {
    expect(Auth).to.be.a.function();
    done();
  });

  it('throws error missing service auth', (done) => {
    expect(() => Auth(null, {})).to.throw('missing form.io service auth object');
    done();
  });

  it('throws error missing user auth object', (done) => {
    expect(() => Auth({}, null)).to.throw('missing form.io user auth object');
    done();
  });

  it('return an object of functions', (done) => {
    expect(auth).to.be.object();
    expect(auth.getServiceUserToken).to.be.a.function();
    expect(auth.getUserJwt).to.be.a.function();
    done();
  });
});
