'use strict';

const Code = require('code');
const Lab = require('lab');
const Logger = require('../logger');
const config = require('../config');
const pack = require('../package.json');
const Server = require('../server');

const lab = exports.lab = Lab.script();
const after = lab.after;
const before = lab.before;
const describe = lab.describe;
const expect = Code.expect;
const it = lab.it;

const logger = Logger(config);
const server = Server(pack, [], logger, config);

describe('Routes', function() {
  before((done) => {
    server.start(() => done());
  });

  after((done) => {
    server.stop(() => done());
  });

  it('GET /documentation', (done) => {
    server.inject('/documentation', (res) => {
      expect(res.statusCode).to.equal(200);
      done();
    });
  });

  it('GET /junk', (done) => {
    server.inject('/junk', (res) => {
      expect(res.statusCode).to.equal(404);
      expect(res.result).to.equal({
        'statusCode': 404,
        'error': 'Not Found'
      });
      done();
    });
  });
});

