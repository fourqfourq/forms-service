/* eslint max-len:0 */
'use strict';

const Auth = require('../formio-auth');
const Code = require('code');
const FormSignature = require('../formio-signature');
const FormioServiceAuth = require('../formio-service-auth');
const FormioUserAuth = require('../formio-user-auth');
const Lab = require('lab');
const Logger = require('../logger');
const config = require('../config');
const formWithSignature = require('./fixtures/form-with-signature.json');
const formWithoutSignature = require('./fixtures/form-without-signature.json');
const mockKnex = require('mock-knex');
const nock = require('nock');
const uuid = require('node-uuid');

const lab = exports.lab = Lab.script();
const afterEach = lab.afterEach;
const before = lab.before;
const beforeEach = lab.beforeEach;
const describe = lab.describe;
const expect = Code.expect;
const it = lab.it;

describe('Signature', () => {
  let auth;
  let cache;
  let db;
  let formioServiceAuth;
  let formioUserAuth;
  let logger;
  let signature;
  let tracker;
  let user;

  const clientId = 1;
  const formioProjectUri = config('FORMIO_PROJECT_URL');
  const formioUri = config('FORMIO_URL');
  const login = config('FORMIO_ROUTE_SERVICE_USER_LOGIN');
  const personId = uuid.v4();
  const token = '0123456789';

  before((done) => {
    tracker = mockKnex.getTracker();
    db = require('../database')(config);
    mockKnex.mock(db);
    user = require('../models/user')(db);
    signature = require('../models/signature')(db);
    logger = Logger(config);
    cache = require('../redis')(logger, config);
    formioServiceAuth = FormioServiceAuth(cache, logger, config);
    formioUserAuth = FormioUserAuth(formioServiceAuth, logger, config, user, cache);
    auth = Auth(formioServiceAuth, formioUserAuth);
    done();
  });

  beforeEach((done) => {
    tracker.install();
    nock.cleanAll();
    done();
  });

  afterEach((done) => {
    tracker.uninstall();
    done();
  });

  it('return a function', (done) => {
    expect(FormSignature).to.be.a.function();
    done();
  });

  it('return an object', (done) => {
    expect(FormSignature({}, {}, {}, {})).to.be.an.object();
    done();
  });

  it('has a function named needsSignature', (done) => {
    const needsSignature = FormSignature({}, {}, {}, {}).needsSignature;
    expect(needsSignature).to.be.a.function();
    done();
  });

  it('throws error for missing signature model', (done) => {
    expect(() => FormSignature(null)).to.throw('missing signature model');
    done();
  });

  it('throws error for missing auth object', (done) => {
    expect(() => FormSignature({}, null)).to.throw('missing authorization object');
    done();
  });

  it('throws error for missing logger object', (done) => {
    expect(() => FormSignature({}, {}, null)).to.throw('missing logger object');
    done();
  });

  it('throws error for missing configuration object', (done) => {
    expect(() => FormSignature({}, {}, {}, null)).to.throw('missing configuration object');
    done();
  });

  it('returns false when form does not require a signature', (done) => {
    const formName = 'test';
    const formSignature = FormSignature(signature, auth, logger, config);

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response(null);
      }
    });

    nock(formioUri)
      .post(login)
      .reply(200, {}, { 'x-jwt-token': token });

    nock(formioProjectUri)
      .get(`/${formName}`)
      .reply(200, formWithoutSignature);

    formSignature.needsSignature(formName, clientId, personId)
      .then((response) => {
        expect(response).to.be.false();
        done();
      });
  });

  it('returns true when form requires a signature and never signed before', (done) => {
    const formName = 'test';
    const formSignature = FormSignature(signature, auth, logger, config);

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response(null);
      }
    });

    nock(formioUri)
      .post(login)
      .reply(200, {}, { 'x-jwt-token': token });

    nock(formioProjectUri)
      .get(`/${formName}`)
      .reply(200, formWithSignature);

    formSignature.needsSignature(formName, clientId, personId)
      .then((response) => {
        expect(response).to.be.true();
        done();
      });
  });

  it('returns false when form requires a signature but already signed and has not expired', (done) => {
    const formName = 'test';
    const formSignature = FormSignature(signature, auth, logger, config);

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        const today = new Date();
        query.response([{
          signedDateTime: today.toISOString()
        }]);
      }
    });

    nock(formioUri)
      .post(login)
      .reply(200, {}, { 'x-jwt-token': token });

    nock(formioProjectUri)
      .get(`/${formName}`)
      .reply(200, formWithSignature);

    formSignature.needsSignature(formName, clientId, personId)
      .then((response) => {
        expect(response).to.be.false();
        done();
      });
  });

  it('returns true when form requires a signature and already signed but signature has expired', (done) => {
    const formName = 'test';
    const formSignature = FormSignature(signature, auth, logger, config);

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response([{
          signedDateTime: '2009-05-15T13:45'
        }]);
      }
    });

    nock(formioUri)
      .post(login)
      .reply(200, {}, { 'x-jwt-token': token });

    nock(formioProjectUri)
      .get(`/${formName}`)
      .reply(200, formWithSignature);

    formSignature.needsSignature(formName, clientId, personId)
      .then((response) => {
        expect(response).to.be.true();
        done();
      });
  });

  it('returns true when form requires a signature, already signed, signature has not expired, but form has been modified since signed', (done) => {
    const formName = 'test';
    const formSignature = FormSignature(signature, auth, logger, config);

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response([{
          signedDateTime: '2009-08-15T13:45'
        }]);
      }
    });

    nock(formioUri)
      .post(login)
      .reply(200, {}, { 'x-jwt-token': token });

    nock(formioProjectUri)
      .get(`/${formName}`)
      .reply(200, formWithSignature);

    formSignature.needsSignature(formName, clientId, personId)
      .then((response) => {
        expect(response).to.be.true();
        done();
      });
  });

  it('rejects promise', (done) => {
    const formSignature = FormSignature(signature, auth, logger, config);

    nock(formioUri)
      .post(login)
      .reply(200, {}, { 'x-jwt-token': token });

    formSignature.needsSignature(null, clientId, personId)
      .catch((error) => {
        expect(error).to.be.an.object();
        done();
      });
  });

  it('returns false when lastModifiedDate does not have a default value', (done) => {
    const formName = 'test';
    const formSignature = FormSignature(signature, auth, logger, config);

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response(null);
      }
    });

    const noContainer = JSON.parse(JSON.stringify(formWithSignature));
    noContainer.components = [{
      hiddenContainer: {
        components: [{
          key: 'lastModifiedDate'
        }]
      }
    }];

    nock(formioUri)
      .post(login)
      .reply(200, {}, { 'x-jwt-token': token });

    nock(formioProjectUri)
      .get(`/${formName}`)
      .reply(200, noContainer);

    formSignature.needsSignature(formName, clientId, personId)
      .then((response) => {
        expect(response).to.be.false();
        done();
      });
  });

  it('returns false when signedDuration does not have a default value', (done) => {
    const formName = 'test';
    const formSignature = FormSignature(signature, auth, logger, config);

    tracker.on('query', (query) => {
      if (query.method === 'select') {
        query.response(null);
      }
    });

    const noContainer = JSON.parse(JSON.stringify(formWithSignature));
    noContainer.components = [{
      hiddenContainer: {
        components: [{
          key: 'signedDuration'
        }]
      }
    }];

    nock(formioUri)
      .post(login)
      .reply(200, {}, { 'x-jwt-token': token });

    nock(formioProjectUri)
      .get(`/${formName}`)
      .reply(200, noContainer);

    formSignature.needsSignature(formName, clientId, personId)
      .then((response) => {
        expect(response).to.be.false();
        done();
      });
  });
});

