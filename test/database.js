'use strict';

const Code = require('code');
const Database = require('../database');
const Lab = require('lab');
const config = require('../config');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const it = lab.it;
const expect = Code.expect;

const database = Database(config);

describe('Database', () => {
  it('has expected config values', (done) => {
    expect(database.client.config).to.equal({
      client: '',
      connection: {
        port: '',
        host: '',
        db: '',
        user: '',
        password: ''
      },
      debug: false,
      pool: {
        min: 2,
        max: 10
      }
    });
    done();
  });
});

