'use strict';

const Code = require('code');
const Hapi = require('hapi');
const Lab = require('lab');
const Logger = require('../logger');
const Server = require('../server');
const config = require('../config');
const pack = require('../package.json');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const expect = Code.expect;
const it = lab.it;
const logger = Logger(config);

describe('Server', () => {
  it('handles an error loading plugins', (done) => {
    const origHapiServer = Hapi.Server;
    const origExit = process.exit;

    Hapi.Server = function() {
      Hapi.Server = origHapiServer;
      return {
        connection: () => {},
        register: (plugins, callback) => callback(new Error('test')),
        route: () => {},
        start: () => {}
      };
    };

    process.exit = function(code) {
      process.exit = origExit;
      expect(code).to.equal(1);
      done();
    };

    Server(pack, [], logger, config).start(() => {});
  });
});
