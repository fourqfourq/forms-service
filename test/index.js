'use strict';

// avoid node warnings while testing
// (node) warning: possible EventEmitter memory leak detected.
// 11 listeners added. Use emitter.setMaxListeners() to increase limit.
process.setMaxListeners(0);

const Code = require('code');
const Lab = require('lab');
const Service = require('..');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const it = lab.it;
const expect = Code.expect;

describe('Service', () => {
  it('executes', (done) => {
    expect(Service).to.be.an.object();
    done();
  });
});

