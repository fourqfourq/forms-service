'use strict';

const crypto = require('crypto');

module.exports = (config) => {
  if (config == null) {
    throw new Error('missing configuration object');
  }

  const algorithm = config('ENCRYPTION_ALG');
  const password = config('ENCRYPTION_PWD');

  function encrypt(value) {
    const cipher = crypto.createCipher(algorithm, password);
    const crypted = cipher.update(value, 'utf8', 'hex');
    return crypted + cipher.final('hex');
  }

  function decrypt(value) {
    const decipher = crypto.createDecipher(algorithm, password);
    const dec = decipher.update(value, 'hex', 'utf8');
    return dec + decipher.final('utf8');
  }

  return {
    encrypt: encrypt,
    decrypt: decrypt
  };
};
